# mini_canvas(API12 - 5.0.3.800)

[mini_canvas]() 是一款极为简单易用的绘制类组件 `MiniCanvas`，它仅需一行代码即可轻松实现 `线条`、`圆形`、`圆环`、`椭圆`、`矩形`、`文本` 和 `图片` 等各种形状的绘制。



## 安装

```shell
ohpm i @nutpi/mini_canvas
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/link?target=https%3A%2F%2Fohpm.openharmony.cn%2F%23%2Fcn%2Fhelp%2Fdownloadandinstall)



## 使用

- **绘制线条**

  ```typescript
  import { ICanvas, MiniCanvas, Paint } from '@nutpi/mini_canvas'

  @Entry
  @ComponentV2
  struct attributeDemo {
  
    build() {
      Column() {
        MiniCanvas({
          onDraw: (canvas: ICanvas) => {
            // 创建画笔
            let paint = new Paint()
            // 绘制默认线条
            canvas.drawLine(10, 10, 180, 10, paint)
  
            // 设置线条粗细
            paint.setStrokeWidth(10)
            // 设置线条颜色
            paint.setColor("#ff0000")
            // 绘制线条
            canvas.drawLine(30, 30, 180, 60, paint)
          }
        })
      }
      .width('100%')
      .height("100%")
    }
  }
  ```

  样例运行结果如下图所示：

  ![0100.png](https://api.nutpi.net/file/topic/2025-01-23/image/0f4575c5af8c4f679daef9999d611f05b1662.png)

- **绘制圆形/圆环**

  ```typescript
  import { ICanvas, MiniCanvas, Paint } from '@nutpi/mini_canvas'

  @Entry
  @ComponentV2
  struct attributeDemo {
  
    build() {
      Column() {
        MiniCanvas({
          onDraw: (canvas: ICanvas) => {
            // 创建画笔
            let paint = new Paint()
            // 绘制默认圆形
            canvas.drawCircle(80, 80, 30, paint)
  
            // 设置圆形颜色
            paint.setColor("#ff0000")
            // 绘制红色圆形
            canvas.drawCircle(150, 80, 30, paint)
  
            // 绘制圆环
            paint.setStroke(true)
            canvas.drawCircle(220, 80, 30, paint)
  
            paint.setStrokeWidth(10)
            canvas.drawCircle(300, 80, 30, paint)
          }
        })
      }
      .width('100%')
      .height("100%")
    }
  }
  ```

  样例运行结果如下图所示：

  ![0101.png](https://api.nutpi.net/file/topic/2025-01-23/image/a97df79ea26f4033a474b2e9321efcc0b1662.png)

- **绘制矩形/矩形框**

  ```typescript
  import { ICanvas, MiniCanvas, Paint } from '@nutpi/mini_canvas'

  @Entry
  @ComponentV2
  struct attributeDemo {
  
    build() {
      Column() {
        MiniCanvas({
          onDraw: (canvas: ICanvas) => {
            // 创建画笔
            let paint = new Paint()
            // 绘制默认矩形
            canvas.drawRect(10, 10, 110, 40, paint)
  
            // 绘制红色矩形
            paint.setColor("#ff0000")
            canvas.drawRect(150, 10, 110, 40, paint)
  
            // 绘制红色矩形框
            paint.setStroke(true)
            canvas.drawRect(10, 70, 110, 40, paint)
  
            // 设置边框大小
            paint.setStrokeWidth(10)
            canvas.drawRect(160, 70, 110, 40, paint)
          }
        })
      }
      .width('100%')
      .height("100%")
    }
  }
  ```

  样例运行结果如下图所示：

  ![0102.png](https://api.nutpi.net/file/topic/2025-01-23/image/cb95b0afa59948f8aa7e3dcb3a10f62eb1662.png)

- 绘制文本

  ```typescript
  import { ICanvas, MiniCanvas, Paint } from '@nutpi/mini_canvas'

  @Entry
  @ComponentV2
  struct attributeDemo {
  
    build() {
      Column() {
        MiniCanvas({
          onDraw: (canvas: ICanvas) => {
            // 创建画笔
            let paint = new Paint()
            // 绘制文本
            canvas.drawText("OpenHarmony", 10, 10, paint)
  
            // 设置字体颜色
            paint.setColor("#ff0000")
            canvas.drawText("OpenHarmony", 180, 10, paint)
  
            // 设置字体大小
            paint.setFontSize(vp2px(30))
            canvas.drawText("OpenHarmony", 10, 50, paint)
  
          }
        })
      }
      .width('100%')
      .height("100%")
    }
  }
  ```

  样例运行结果如下图所示：

  ![0103.png](https://api.nutpi.net/file/topic/2025-01-23/image/9945214dc72744afbc890dc0084f6baab1662.png)

- **绘制图片**

  ```typescript
  import { ICanvas, MiniCanvas, Paint } from '@nutpi/mini_canvas'

  @Entry
  @ComponentV2
  struct attributeDemo {
  
    build() {
      Column() {
        MiniCanvas({
          onDraw: (canvas: ICanvas) => {
            // 创建画笔
            let paint = new Paint()
  
            // 绘制图片
            let bitmap = new ImageBitmap("pages/test.jpg")
            canvas.drawImage(bitmap, 0, 0, bitmap.width, bitmap.height, 0, 0, 400, 200, paint)
          }
        })
      }
      .width('100%')
      .height("100%")
    }
  }
  ```

  样例运行结果如下图所示：

  ![0104.png](https://api.nutpi.net/file/topic/2025-01-23/image/819b1fe77fc64ad8a0673c916a179881b1662.png)



## 反馈

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/ark-ui/MiniCanvas/issues) 给我们；
当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/ark-ui/MiniCanvas/pulls) 。


## QQ交流群

[695438501](https://qm.qq.com/cgi-bin/qm/qr?k=YBDKMU9Lt309QL_I1Lfa2jVpGwx65VSR&jump_from=webapi)



## LICENSE

本项目基于 [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) ，在拷贝和借鉴代码时，请大家务必注明出处。
