import { Paint } from "./paint";

/**
 * 画布的抽象接口，对外提供画布的具体能力
 */
export interface ICanvas {

  /**
   * 绘制直线
   * @param startX：起始点X坐标
   * @param startY：起始点Y坐标
   * @param stopX：结束点X坐标
   * @param stopY：结束点Y坐标
   * @param paint：绘制直线的画笔
   */
  drawLine(startX: number, startY: number, stopX: number, stopY: number, paint: Paint): void;

  /**
   * 绘制文本
   * @param text：待绘制的文本内容
   * @param x：绘制文本的起始点X坐标
   * @param y：绘制文本的起始点Y坐标
   * @param paint：绘制文本的画笔
   * @param maxWidth：绘制文本的最大宽度，超过该宽度不再绘制
   */
  drawText(text: string, x: number, y: number, paint: Paint, maxWidth?: number): void;

  /**
   * 绘制圆
   * @param x：圆心X轴坐标
   * @param y：圆心Y轴坐标
   * @param radius：圆的半径
   * @param paint：绘制圆的画笔
   */
  drawCircle(x: number, y: number, radius: number, paint: Paint): void;

  /**
   * 绘制椭圆
   * @param x：绘制椭圆的起始点X轴坐标
   * @param y：绘制椭圆的起始点Y轴坐标
   * @param width：椭圆外接矩形宽度
   * @param height：椭圆外接矩形高度
   * @param paint：绘制椭圆的画笔
   */
  drawOval(x: number, y: number, width: number, height: number, paint: Paint): void;

  /**
   * 绘制圆弧
   * @param x：圆弧所在圆的圆心X轴坐标
   * @param y：圆弧所在圆的圆心Y轴坐标
   * @param radius：圆弧所在圆的半径
   * @param startAngle：圆弧开始角度
   * @param endAngle：圆弧结束角度
   * @param paint：绘制圆弧的画笔
   * @param counterclockwise：是否逆时针绘制，默认为false，表示顺时针绘制
   */
  drawArc(x: number, y: number, radius: number, startAngle: number, endAngle: number, paint: Paint, counterclockwise?: boolean): void;

  /**
   * 绘制矩形
   * @param x：矩形的起始点X轴坐标
   * @param y：矩形的起始点Y轴坐标
   * @param width：矩形的宽度
   * @param height：矩形的高度
   * @param paint：绘制矩形的画笔
   */
  drawRect(x: number, y: number, width: number, height: number, paint: Paint): void;

  /**
   * 绘制圆角矩形
   * @param x：圆角矩形的起始点X轴坐标
   * @param y：圆角矩形的起始点Y轴坐标
   * @param width：圆角矩形的宽度
   * @param height：圆角矩形的高度
   * @param radius：矩形的圆角大小
   * @param paint：绘制圆角矩形的画笔
   */
  drawRoundRect(x: number, y: number, width: number, height: number, radius: number, paint: Paint): void;

  /**
   * 绘制图片
   *
   * @param image：待绘制的图片
   * @param sx：从图片的左侧sx处开始绘制
   * @param sy：从图片的顶部sy处开始绘制
   * @param sWidth：选取待绘制图片的绘制宽度
   * @param sHeight：选取待绘制图片的绘制高度
   * @param dx：图片在画布X轴坐标
   * @param dy：图片在画布Y轴坐标
   * @param dWidth：图片在画布上绘制的宽度
   * @param dHeight：图片在画布上绘制的高度
   * @param paint：绘制图片的画笔
   */
  drawImage(image: ImageBitmap | PixelMap, sx: number, sy: number, sWidth: number, sHeight: number, dx: number, dy: number, dWidth: number, dHeight: number, paint: Paint): void;

  /**
   * 绘制图片
   *
   * @param image：待绘制的图片
   * @param dx：图片在画布X轴坐标
   * @param dy：图片在画布Y轴坐标
   * @param paint：绘制图片的画笔
   */
  drawPicture(image: ImageBitmap | PixelMap, dx: number, dy: number, paint: Paint): void;

  /**
   * 绘制图片
   *
   * @param image：待绘制的图片
   * @param dx：图片在画布X轴坐标
   * @param dy：图片在画布Y轴坐标
   * @param dw：图片在画布上绘制的宽度
   * @param dh：图片在画布上绘制的高度
   * @param paint：绘制图片的画笔
   */
  drawBitmap(image: ImageBitmap | PixelMap, dx: number, dy: number, dw: number, dh: number, paint: Paint): void;

  /**
   * 测量文本，可以获取文本宽度、高度等信息
   * @param text：带测量文本内容
   * @param paint：画布的画笔
   */
  measureText(text: string, paint: Paint): TextMetrics;

  /**
   * 顺时针旋转画布
   * @param x：旋转的X坐标
   * @param y：旋转的Y坐标
   * @param angle：旋转画布的角度
   * @param paint：画布的画笔
   */
  rotate(x: number, y: number, angle: number, paint: Paint): void;

  /**
   * 放缩画布，后续的绘制操作将按照缩放比例进行缩放
   * @param x：画布在X轴上的放缩值
   * @param y：画布在Y轴上的放缩值
   * @param paint：画布的画笔
   */
  scale(x: number, y: number, paint: Paint): void;

  /**
   * 移动画布坐标系的原点，后续的绘制操作将按照新坐标点进行绘制
   * @param x：设置X轴上的偏移量
   * @param y：设置Y轴上的偏移量
   * @param paint: 画布的画笔
   */
  translate(x: number, y: number, paint: Paint): void;

  /**
   * 恢复保存的绘图上下文
   * @param paint: 画布的画笔
   */
  restore(paint: Paint): void;

  /**
   * 保存绘图的上下文
   */
  save(paint: Paint): void;

  /**
   * 清空画布
   */
  clear(): void;

  /**
   * 画布内容保存为一张图片
   */
  getImageData(): ImageData;

  /**
   * 画布内容保存为一张图片
   */
  getPixelMap(): PixelMap;

  /**
   * 获取画布的宽度
   */
  getWidth(): number

  /**
   * 获取画布的高度
   */
  getHeight(): number

  /**
   * 获取画布ID
   */
  getID(): string
}
